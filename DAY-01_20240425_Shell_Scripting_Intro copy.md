##### Session Video  

```
https://drive.google.com/file/d/1LFc6yl03konEek7YqH0qXesP8fC7q6Y8/view?usp=sharing
```

## What is Shell Scripting?

Shell scripting is a method of automating tasks and controlling the behavior of a Linux or Unix-like operating system. A shell script is a plain text file containing a series of commands that can be executed by a shell interpreter (such as Bash, Zsh, or others). These commands can perform a variety of tasks, from basic file operations to complex system management tasks.

## Applications of Shell Scripting

- **Automation**: Shell scripts can automate repetitive tasks, such as data backups, file transfers, or log monitoring. Automation saves time and reduces the risk of human error.

- **System Management**: Shell scripts are widely used for system administration tasks such as managing processes, monitoring system resources, and configuring servers.

- **Task Scheduling**: Shell scripts can be scheduled to run at specific times using cron jobs, making them useful for tasks like database maintenance and batch processing.

- **Data Processing**: Scripts can handle data manipulation, such as sorting, filtering, and transforming data files.

- **Integration**: Shell scripts can integrate different software tools and services, acting as a glue to combine functionality from various sources.

- **Development**: Shell scripts are useful in software development for automating build processes, running tests, and deploying applications.

## Role of the Shell as an Interpreter

The shell acts as an intermediary between the user and the operating system. It interprets the commands entered by the user or contained in scripts and executes them. The shell's role can be summarized as follows:

- **Command Execution**: The shell interprets the user's commands and executes them, whether they are entered interactively or as part of a script.

- **Command Chaining**: The shell allows chaining commands together using operators such as `;` (sequence), `&&` (logical AND), and `||` (logical OR).

- **Environment Management**: The shell manages the environment in which commands run, including variables, paths, and the current working directory.

- **Redirection and Piping**: Shells support input and output redirection (`<`, `>`, `>>`) and piping (`|`) to chain commands and manipulate data streams.

- **Control Structures**: Shells provide control structures (e.g., if-else statements and loops) that enable scripts to perform conditional logic and iterative tasks.

By understanding how the shell works and how to write shell scripts, you can harness the power of the operating system to perform complex tasks efficiently.

## Understanding the Shebang

The shebang, also known as the hashbang (`#!`), is the very first line at the top of a script file that specifies which shell or interpreter should be used to execute the script. It is a crucial part of a shell script because it ensures the script is run by the correct interpreter, regardless of the default shell set on the system.

### Specifying the Shell

The shebang line consists of the `#!` characters followed by the path to the desired shell or interpreter. For example:

- **Bash**:
    - `#!/bin/bash` specifies that the script should be executed using the Bash shell, which is the default shell on many Linux distributions.

- **Zsh**:
    - `#!/bin/zsh` specifies that the script should be executed using the Zsh shell.

Other shells such as **sh** (`#!/bin/sh`) or **Python** (`#!/usr/bin/python3`) can also be specified, depending on the script's requirements.

### Purpose of the Shebang

- **Specify the Interpreter**: The shebang line tells the operating system which interpreter should be used to execute the script.
  
- **Portability**: By explicitly specifying the interpreter, you ensure the script will run consistently on different systems, regardless of the user's default shell.
  
- **Avoid Errors**: Specifying the shell avoids potential issues if the script contains syntax or features specific to a particular shell.

### Examples

Here are some examples of shebang lines and their purposes:

- **Bash**:
    - `#!/bin/bash`: This is the most common shebang for scripts that use Bash-specific syntax and features.

- **Zsh**:
    - `#!/bin/zsh`: Use this shebang if the script contains Zsh-specific syntax or commands.

### Practical Tip

- **Verify the Shell Path**: Before writing a script, verify the path to the shell interpreter you want to use by running the command `which bash` or `which zsh` in the terminal.
  
- **Start with a Shebang**: Always start your scripts with the appropriate shebang line for the shell you intend to use.

By understanding the shebang and its purpose, students can write scripts that run consistently and effectively across different environments.

## Exploring Different Shells

In Linux and Unix-like operating systems, there are several different shells available, each with its own syntax and set of features. Here's an overview of some common shells:

### Bash (Bourne Again Shell)

- **Overview**: Bash is the most widely used shell in Linux distributions and serves as the default shell for many systems. It is known for its compatibility with the Bourne shell (sh) while offering numerous enhancements.
- **Features**:
    - User-friendly scripting syntax.
    - Command history and completion.
    - Advanced scripting features such as functions, arrays, and regular expressions.
- **Learning Choice**: Bash is an excellent shell for beginners to learn due to its wide usage, compatibility, and rich set of features.

### Zsh (Z Shell)

- **Overview**: Zsh is another popular shell known for its rich feature set and extensive customization options.
- **Features**:
    - Advanced tab completion and auto-suggestions.
    - Scripting features similar to Bash, with some additional syntax and options.
    - Customizable prompt and theme options.
- **Use Case**: Zsh is a great choice for more experienced users who appreciate its advanced features and customization.

### Other Shells

- **Fish**:
    - **Overview**: Fish (Friendly Interactive Shell) is known for its user-friendly and intuitive features.
    - **Features**:
        - Syntax highlighting and autosuggestions.
        - Readable and easy-to-understand syntax.
        - Strong default configurations.
    - **Use Case**: Fish is a good choice for users looking for an alternative interactive shell with out-of-the-box features.

- **tcsh and csh**:
    - **Overview**: These shells are known for their C-like syntax.
    - **Features**:
        - C-like scripting syntax.
        - Features such as aliasing and job control.
    - **Use Case**: Suitable for users familiar with C syntax or those working with legacy systems.

## Encouragement to Learn Bash

- **Wide Adoption**: Bash is the most commonly used shell in Linux and is widely supported in various environments.
- **Community Support**: There is a vast amount of resources and community support available for learning Bash.
- **Compatibility**: Bash scripts tend to be more portable and compatible across different systems compared to scripts written in other shells.


## How to Change Between the Shells

Changing between different shells in a Linux or Unix-like operating system is a straightforward process. You can switch shells temporarily in your current terminal session, or you can change your default shell for your user account. Here's how to do both:

### Temporarily Switching Shells

You can temporarily switch to a different shell in your current terminal session by simply typing the shell's name and pressing Enter. For example:

- **Switch to Bash**:
    - Type `bash` and press Enter.

- **Switch to Zsh**:
    - Type `zsh` and press Enter.

- **Switch to sh**:
    - Type `sh` and press Enter.

Once you've switched to a new shell, your prompt will change to reflect the new shell. You can check your current shell using the command `echo $SHELL`.

### Returning to Your Previous Shell

- **Exit the current shell**:
    - To return to your previous shell, type `exit` and press Enter. This will close the current shell and return you to the previous one.

### Changing Your Default Shell

To permanently change your default shell for your user account:

1. **Check available shells**:
    - You can check the available shells on your system by running the command `chsh -l` or `cat /etc/shells`.
  
2. **Change your default shell**:
    - Run the command `chsh` (change shell) in the terminal.
    - You may be prompted to enter your password.
    - You will be asked to enter the path of the new shell (e.g., `/bin/bash` for Bash or `/bin/zsh` for Zsh).
  
3. **Log out and log back in**:
    - To apply the changes, log out of your session and log back in. The next time you open a terminal, your new default shell will be the one you specified.

### Example Command

- To change your default shell to Bash:
    ```bash
    chsh -s /bin/bash
    ```

- To change your default shell to Zsh:
    ```bash
    chsh -s /bin/zsh
    ```

### Notes

- **Permission**: You may need superuser permissions to change the default shell for a user other than yourself.
  
- **Verification**: After changing your default shell, verify the change by checking the value of `$SHELL` in a new terminal session.

By following these steps, you can easily switch between different shells and set your preferred shell as your default.

## Setting Up a Script File

Creating and running a basic shell script involves several steps: writing the script, saving it as a file, and making it executable. Follow the guide below to set up a script file:

### Step 1: Create a New Script File

1. **Open a Text Editor**: Use a text editor such as `nano`, `vim`, or `gedit` to create a new script file.
    - For example, to open a file named `script.sh` using `nano`, run the command:
        ```bash
        vi script.sh
        ```

2. **Write the Shebang Line**: At the top of the script file, include the shebang line to specify the shell interpreter for the script.
    - For example, for a Bash script, use:
        ```bash
        #!/bin/bash
        ```

3. **Write Your Script**: Add your desired script commands below the shebang line. For instance:
    ```bash
    echo "Hello, World!"
    ```

4. **Save and Close the File**: Once you've finished writing your script, save the file and exit the text editor.

### Step 2: Make the Script Executable

1. **Change Permissions**: Make the script file executable by using the `chmod` command:
    ```bash
    chmod +x script.sh
    ```

2. **Verify Permissions**: To verify that the script is executable, you can use the `ls -l` command to check the file permissions:
    ```bash
    ls -l script.sh
    ```

    - The file permissions will now include an 'x' indicating it is executable (e.g., `-rwxr-xr-x`).

### Step 3: Run the Script

1. **Execute the Script**: You can run the script using its relative or absolute path.
    - For a script in the current directory, prefix the script name with `./`:
        ```bash
        ./script.sh
        ```

2. **Check the Output**: Observe the output of the script in the terminal.

### Importance of Making the Script Executable

- **Running the Script**: Making the script executable allows you to run it directly from the terminal.
- **Portability**: An executable script can be easily shared and run on other systems.
- **Efficiency**: Executable scripts can be scheduled or triggered by other scripts or services.

