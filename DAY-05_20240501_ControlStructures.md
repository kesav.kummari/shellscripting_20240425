##### Session Video  

```
https://drive.google.com/file/d/1HySOg9zpvyo8iPoIInSeE4f1h0SadAO2/view?usp=sharing
```

# Day 5: Control Structures and Loops

### Topics Covered:
- **If-else statements**: Learn how to use conditional statements to control the flow of a script based on different conditions.

### Activities:
- Practice writing simple if-else conditions using comparison operators.

### Content:
Control structures, such as `if` statements and loops, allow you to control the flow of your script based on different conditions or iterations. Here are some types of `if` statements and the `switch` statement in bash programming, along with their syntaxes and examples:

#### `if` Statement

- **Definition**: An `if` statement executes a block of code if a given condition is true.
- **Syntax**:
    ```bash
    if [ condition ]; then
        # Code to execute if condition is true
    fi
    ```
![If Statement](images/ifstatement.png "if")

    

- **Example**:
    ```bash
    number=10
    if [ $number -gt 5 ]; then
        echo "The number is greater than 5."
    fi
    ```

#### `if-else` Statement

- **Definition**: An `if-else` statement executes one block of code if a condition is true and another block if the condition is false.
- **Syntax**:
    ```bash
    if [ condition ]; then
        # Code to execute if condition is true
    else
        # Code to execute if condition is false
    fi
    ```
![Ifelse Statement](images/ifelse.png "if")
- **Example**:
    ```bash
    number=3
    if [ $number -gt 5 ]; then
        echo "The number is greater than 5."
    else
        echo "The number is not greater than 5."
    fi
    ```

#### `if-elif-else` Statement (Else If Ladder)

- **Definition**: An `if-elif-else` statement allows for multiple conditions to be checked in sequence. The script executes the code block corresponding to the first true condition.
- **Syntax**:
    ```bash
    if [ condition1 ]; then
        # Code to execute if condition1 is true
    elif [ condition2 ]; then
        # Code to execute if condition2 is true
    else
        # Code to execute if none of the above conditions are true
    fi
    ```
![elseIf ladder Statement](images/elseifladder.png "if")
- **Example**:
    ```bash
    score=75
    if [ $score -ge 90 ]; then
        echo "Grade: A"
    elif [ $score -ge 80 ]; then
        echo "Grade: B"
    elif [ $score -ge 70 ]; then
        echo "Grade: C"
    else
        echo "Grade: F"
    fi
    ```

#### Nested `if` Statements

- **Definition**: Nested `if` statements allow you to embed an `if` statement within another `if` statement to handle more complex conditions.
- **Syntax**:
    ```bash
    if [ condition1 ]; then
        if [ condition2 ]; then
            # Code to execute if both condition1 and condition2 are true
        else
            # Code to execute if condition1 is true but condition2 is false
        fi
    else
        # Code to execute if condition1 is false
    fi
    ```
![nestedIf Statement](images/nestedifelse.png "if")
- **Example**:
    ```bash
    age=30
    income=50000
    if [ $age -ge 25 ]; then
        if [ $income -ge 40000 ]; then
            echo "You qualify for the loan."
        else
            echo "You do not qualify due to insufficient income."
        fi
    else
        echo "You do not qualify due to age restriction."
    fi
    ```

#### `case` Statement (Switch Statement)

- **Definition**: The `case` statement allows you to match a variable against different patterns and execute a corresponding block of code based on the matched pattern.
- **Syntax**:
    ```bash
    case variable in
        pattern1)
            # Code to execute if variable matches pattern1
            ;;
        pattern2)
            # Code to execute if variable matches pattern2
            ;;
        *)
            # Code to execute if variable does not match any pattern
            ;;
    esac
    ```
![case Statement](images/switchcase.png "if")
- **Example**:
    ```bash
    day="Monday"
    case $day in
        "Monday")
            echo "It's the start of the week!"
            ;;
        "Friday")
            echo "It's the end of the week!"
            ;;
        *)
            echo "It's just another day."
            ;;
    esac
    ```


