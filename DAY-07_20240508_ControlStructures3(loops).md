##### Session Video  

```
https://drive.google.com/file/d/1fH8st3TdtX7YGHOskQFJNrXWOBQva8Nn/view?usp=sharing
```

## Day7: Control Structures and Loops (cont'd)

### Topics Covered: For and while loops

In this session, you will learn how to use `for` and `while` loops in shell scripts to iterate over sequences and perform repeated operations. This includes definitions, syntax, and examples for each type of loop.

### `for` Loops

- **Definition**: A `for` loop is used to iterate over a list of values or a sequence of numbers and execute a block of code for each item.
- **Syntax**:
    ```bash
    for variable in list; do
        # Code to execute for each item in the list
    done
    ```

- **Examples**:
    - **Iterating through a list of names**:
        ```bash
        for name in Alice Bob Charlie; do
            echo "Hello, $name!"
        done
        ```
    - **Iterating through a range of numbers**:
        ```bash
        for i in {1..5}; do
            echo "Number: $i"
        done
        ```
    - **Iterating through a sequence with a step value**:
        ```bash
        for i in {10..0..-2}; do
            echo "Counting down: $i"
        done
        ```

### `while` Loops

- **Definition**: A `while` loop executes a block of code repeatedly as long as a specified condition remains true.
- **Syntax**:
    ```bash
    while [ condition ]; do
        # Code to execute while the condition is true
    done
    ```

- **Examples**:
    - **Looping while a condition is true**:
        ```bash
        count=1
        while [ $count -le 5 ]; do
            echo "Count: $count"
            count=$((count + 1))
        done
        ```
    - **Looping through user input until the user enters "exit"**:
        ```bash
        input=""
        while [ "$input" != "exit" ]; do
            read -p "Enter a command (or 'exit' to quit): " input
            echo "You entered: $input"
        done
        ```
    - **Looping through a range of numbers while incrementing a variable**:
        ```bash
        number=0
        while [ $number -lt 10 ]; do
            echo "Current number: $number"
            number=$((number + 2))
        done
        ```

### Conclusion

Both `for` and `while` loops are essential tools for automating repetitive tasks and iterating over sequences in shell scripting. By practicing writing scripts with both types of loops, you can understand their use cases and how to effectively apply them in different scenarios.
