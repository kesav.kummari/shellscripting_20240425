##### Session Video  

```
https://drive.google.com/file/d/1CklwTk4BZxG4gNcIBMlFUsUP5LgdVRIz/view?usp=sharing
```

# Day 4: Operators in Shell Scripts


### Topics Covered:

1. **Arithmetic Operations**:
    - Perform arithmetic operations using arithmetic expansion (`$((...))`) and different arithmetic operators (`+`, `-`, `*`, `/`, `%`).

2. **String Concatenation**:
    - Combine multiple strings or variables to form a single string.

3. **Basic Assignment Operator**:
    - Use the assignment operator (`=`) to assign values to variables.

4. **Comparison Operators**:
    - Compare values using different comparison operators.

### Concepts and Examples:

#### Arithmetic Operations:

- **Arithmetic Expansion**:
    - **Definition**: Arithmetic expansion allows you to perform arithmetic calculations within a shell script.
    - Syntax: `result=$((expression))`
    - Example:
        ```bash
        num1=10
        num2=5
        sum=$((num1 + num2))
        echo "Sum: $sum"  # Outputs: Sum: 15
        ```

- **Arithmetic Operators**:
    - **Definition**: Operators used for arithmetic calculations.
    - Table of arithmetic operators:

    | Operator | Description                            | Example             | Result |
    | -------- | -------------------------------------- | -------------------- | ------ |
    | `+`      | Addition                               | `$((10 + 5))`        | 15     |
    | `-`      | Subtraction                            | `$((10 - 5))`        | 5      |
    | `*`      | Multiplication                         | `$((10 * 5))`        | 50     |
    | `/`      | Division                               | `$((10 / 5))`        | 2      |
    | `%`      | Modulus (remainder of division)        | `$((10 % 3))`        | 1      |

- **Examples**:
    - Subtract, multiply, and divide numbers:
        ```bash
        num1=15
        num2=3
        difference=$((num1 - num2))
        product=$((num1 * num2))
        quotient=$((num1 / num2))
        echo "Difference: $difference"  # Outputs: Difference: 12
        echo "Product: $product"        # Outputs: Product: 45
        echo "Quotient: $quotient"      # Outputs: Quotient: 5
        ```

    - **Modulus Operator**:
        - **Definition**: The modulus operator (`%`) calculates the remainder when one number is divided by another.
        - Example:
            ```bash
            num1=17
            num2=5
            remainder=$((num1 % num2))
            echo "Remainder: $remainder"  # Outputs: Remainder: 2
            ```

#### String Concatenation:

- **Concatenating Strings**:
    - **Definition**: Concatenating strings means combining multiple strings or variables to form a single string.
    - Example:
        ```bash
        str1="Hello"
        str2="World"
        combined_str="$str1, $str2!"
        echo "$combined_str"  # Outputs: Hello, World!
        ```

- **Concatenation with Variables**:
    - Combine strings and variables together.
    - Example:
        ```bash
        greeting="Hi"
        name="Alice"
        message="$greeting, $name!"
        echo "$message"  # Outputs: Hi, Alice!
        ```

- **Using the `+=` Operator**:
    - **Definition**: Use the `+=` operator to append a string to another string and assign the result to the first variable.
    - Example:
        ```bash
        str1="Hello"
        str2="World"
        str1+=", $str2!"
        echo "$str1"  # Outputs: Hello, World!
        ```

#### Basic Assignment Operator:

- **Definition**: The assignment operator (`=`) is used to assign values to variables.
- Example:
    ```bash
    my_var=42
    my_str="Hello, Shell!"
    ```

#### Comparison Operators:

- **Definition**: Operators used to compare values in conditional statements.
- Table of comparison operators:

| Operator | Description                   | Example        | Meaning         |
| -------- | ----------------------------- | -------------- | --------------- |
| `-eq`    | Equals                         | `[ 10 -eq 10 ]` | True if equal    |
| `-ne`    | Not equal                      | `[ 10 -ne 5 ]`  | True if not equal|
| `-gt`    | Greater than                   | `[ 10 -gt 5 ]`  | True if greater  |
| `-ge`    | Greater than or equal          | `[ 10 -ge 10 ]` | True if >=       |
| `-lt`    | Less than                      | `[ 5 -lt 10 ]`  | True if less     |
| `-le`    | Less than or equal             | `[ 10 -le 10 ]` | True if <=       |

- **Example**: Use comparison operators in an `if` statement:
    ```bash
    value=10
    if [ $value -eq 10 ]; then
        echo "Value is 10."
    else
        echo "Value is not 10."
    fi
    ```
- **Logical Operators**:
    - **Definition**: Operators used to combine multiple conditions.
    - Table of logical operators:

    | Operator | Description                    | Example                   | Meaning                               |
    | -------- | ------------------------------ | -------------------------- | ------------------------------------- |
    | `&&`     | Logical AND                    | `[ $value -eq 10 ] && ...` | True if both conditions are true      |
    | `||`     | Logical OR                     | `[ $value -eq 10 ] || ...` | True if at least one condition is true|

### Practice Exercises:

1. **Perform Arithmetic Operations**:
    - Calculate the sum, difference, product, and quotient of given numbers using arithmetic expansion.

2. **Find the Modulus**:
    - Use the modulus operator to find the remainder of a division.

3. **Concatenate Strings**:
    - Combine multiple strings to form a greeting or a full sentence.

4. **Use the `+=` Operator**:
    - Append a string to another string using the `+=` operator and display the result.

5. **Use Comparison Operators**:
    - Practice using different comparison operators in conditional statements.