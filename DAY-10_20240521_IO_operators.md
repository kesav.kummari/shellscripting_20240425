##### Session Video  

```
https://drive.google.com/file/d/1iduqfQ6XBwZPR51cKrd3rscfV-B7wCDi/view?usp=sharing
```

## Topics Covered: Basic File I/O in Shell Scripts

### Objectives:
- Understand how to perform basic file I/O operations in shell scripts.
- Learn to read from and write to files using various commands.
- Practice using file I/O commands like `cat`, `echo`, `touch`, and redirection operators.

### Key Concepts:

#### 1. Creating Files
- **`touch`**: Create an empty file or update the timestamp of an existing file.
  ```bash
  touch myfile.txt


Writing to Files
- **echo**: Write text to a file using the > or >> operators.
  ```bash
  echo "Hello, World!" > myfile.txt # Overwrites the file
  echo "Appending this line." >> myfile.txt # Appends to the file
  ```
- **Redirection**: Use redirection operators to write the output of commands to a file.
  ```bash
  ls -l > filelist.txt # Save the output of ls -l to filelist.txt
  ```

Reading from Files
- **cat**: Display the contents of a file.
  ```bash
  cat myfile.txt
  ```
- **<**: Use input redirection to read from a file.
  ```bash
  while read line; do
  echo $line
  done < myfile.txt
  ```

Editing Files
- **sed**: Stream editor for filtering and transforming text in files.
  ```bash
  Replace 'oldtext' with 'newtext' in myfile.txt
  sed -i 's/oldtext/newtext/g' myfile.txt

  Display the changes without modifying the file
  sed 's/oldtext/newtext/g' myfile.txt
  ```

Searching in Files
- **grep**: Search for patterns within files.
  ```bash
  Search for the word 'text' in myfile.txt
  grep 'text' myfile.txt

  Search for the word 'text' and display the line numbers
  grep -n 'text' myfile.txt
  ```

Examples:
1. **Creating and Writing to a File**
  ```bash
  #!/bin/bash

  Create a file
  touch example.txt

  Write to the file
  echo "This is an example file." > example.txt
  echo "It contains some text data." >> example.txt
  echo "End of file." >> example.txt
  ```

2. **Reading from a File**
  ```bash
  #!/bin/bash

  Read and display the contents of the file
  cat example.txt

  Another way to read the file line by line
  while read line; do
  echo $line
  done < example.txt
  ```

3. **Using sed to Edit a File**
  ```bash
  #!/bin/bash

  #Replace 'example' with 'sample' in example.txt
  sed -i 's/example/sample/g' example.txt

  Display the updated file
  cat example.txt
  ```
