##### Session Video  

```
https://drive.google.com/file/d/1NksB8SVL23lY3932AZ_VfsnrfvE_M1Jn/view?usp=sharing
```

# Day 2: Variables in Shell Scripts

## Student Notes

### Topics Covered:
- **Declaring Variables**: Learn how to declare variables in shell scripts and understand the rules for variable names.
- **Reading User Input**: Learn how to read input from the user using the `read` command and store it in variables.
- **Manipulating Variables**: Explore different operations that can be performed on variables, such as arithmetic, string manipulation, and conditional checks.
- **Variable Expansion**: Learn how to use variable expansion (`$` and `${}`) in scripts to access the value of variables.

### Explanations and Examples:

1. **Declaring Variables**:
    - **What is a Variable?**:
        - A variable is a named storage location that can hold a value.
        - You can use variables to store data such as numbers, strings, and boolean values.
    - **Rules for Variable Names**:
        - Variables must start with a letter or underscore (`_`) and can contain letters, numbers, and underscores.
        - Variable names are case-sensitive (`myVar` and `myvar` are different variables).
    - **Declaring Variables**:
        - Declare a variable using simple assignment:
            ```bash
            name="Alice"
            age=25
            ```
        - **Example**: Declare additional variables:
            ```bash
            city="New York"
            is_student=true
            ```

2. **Reading User Input**:
    - **The `read` Command**:
        - The `read` command prompts the user for input and stores it in a variable.
        - **Example**: Use `read` to prompt the user for their name and store it in the `user_name` variable:
            ```bash
            read -p "Enter your name: " user_name
            ```
    - **Using User Input**:
        - You can use the user's input in a script to perform different operations.
        - **Example**: Read the user's name and greet them:
            ```bash
            echo "Hello, $user_name!"
            ```
    - **Another Example**: Read multiple inputs and perform some operations with them:
        ```bash
        read -p "Enter your age: " user_age
        if [ $user_age -ge 18 ]; then
            echo "You are an adult!"
        else
            echo "You are a minor!"
        fi
        ```

3. **Manipulating Variables**:
    - **Arithmetic Operations**:
        - Perform arithmetic operations using variables and the arithmetic expansion syntax (`$((...))`):
            ```bash
            num1=10
            num2=5
            sum=$((num1 + num2))
            echo "Sum: $sum"
            ```
        - **Example**: Calculate the product of two numbers and display the result:
            ```bash
            num1=8
            num2=7
            product=$((num1 * num2))
            echo "Product: $product"
            ```
    - **String Manipulation**:
        - Concatenate strings using variables:
            ```bash
            first_name="John"
            last_name="Doe"
            full_name="$first_name $last_name"
            echo "Full Name: $full_name"
            ```
        - **Example**: Create a greeting message by concatenating strings and variables:
            ```bash
            greeting="Good morning"
            full_message="$greeting, $full_name!"
            echo $full_message
            ```
    - **Conditional Checks**:
        - Conditional statements (`if` and `else`) with variables:
            ```bash
            age=20
            if [ $age -ge 18 ]; then
                echo "You are an adult."
            else
                echo "You are a minor."
            fi
            ```
        - **Example**: Check if a number is even or odd using a conditional statement:
            ```bash
            number=7
            if [ $((number % 2)) -eq 0 ]; then
                echo "$number is even."
            else
                echo "$number is odd."
            fi
            ```

4. **Variable Expansion**:
    - **Simple Expansion**:
        - Use `$variable` to access the value of a variable:
            ```bash
            greeting="Hello"
            echo "$greeting, World!"
            ```
        - **Example**: Combine variable expansion with string concatenation:
            ```bash
            person="Alice"
            echo "Welcome to the party, $person!"
            ```
    - **Advanced Expansion**:
        - Use `${variable}` for more control over variable expansion:
            ```bash
            var="Hello, world!"
            echo "${var:7}" # Outputs "world!"
            ```
        - **Example**: Use variable expansion to extract a substring:
            ```bash
            message="Welcome to Shell Scripting!"
            substring="${message:11:5}" # Extracts "Shell"
            echo "Substring: $substring"
            ```

### Practice Exercises:

1. **Variable Declaration and Display**:
    - Declare variables and display their values using `echo`.
2. **Reading User Input**:
    - Use the `read` command to obtain user input and store it in variables.
3. **Manipulating Variables**:
    - Practice arithmetic operations, string concatenation, and conditional checks using variables.
4. **Variable Expansion**:
    - Practice using different forms of variable expansion in scripts.
