##### Session Video  

```
https://drive.google.com/file/d/1wbC-vANXaA9eRpEgMseKveNPnMKqDfeq/view?usp=sharing
```

## Day 9: Advanced Concepts

### Function Parameters

- Functions can accept parameters (arguments), allowing you to pass data to the function when you call it.
- **Syntax**:
    ```bash
    function_name(param1 param2 ...) {
        # Code block using parameters
    }
    ```

- **Example**: Function that accepts a name as a parameter and prints a personalized greeting:
    ```bash
    greet_user() {
        local name=$1
        echo "Hello, $name! Welcome to shell scripting!"
    }
    greet_user "Alice"
    ```

### Special Variables for Arguments

- When a function or script is executed, the arguments provided to it can be accessed using special variables:

    - **`$0`**: The name of the script or function being executed.
    - **`$1`, `$2`, ...**: The positional arguments passed to the script or function. `$1` is the first argument, `$2` is the second, and so on.
    - **`$#`**: The number of arguments passed to the script or function.
    - **`$@`**: All the arguments passed to the script or function as separate words.
    - **`$*`**: All the arguments passed to the script or function as a single string.

- **Example**: Using special variables to access arguments in a function:
    ```bash
    print_args() {
        echo "Script name: $0"
        echo "First argument: $1"
        echo "Second argument: $2"
        echo "All arguments: $@"
        echo "Number of arguments: $#"
    }
    print_args "arg1" "arg2"
    ```

### Returning Values

- Functions can return values using the `return` statement. The return value is stored in the `$?` variable.
- **Syntax**:
    ```bash
    function_name() {
        # Code block
        return value
    }
    ```

- **Example**: Function that adds two numbers and returns the result:
    ```bash
    add_numbers() {
        local num1=$1
        local num2=$2
        return $((num1 + num2))
    }
    add_numbers 3 4
    echo "The sum is: $?"
    ```

### Use Cases

1. **Modularizing Code**:
    - Functions allow you to divide your script into smaller, more manageable parts.
    - **Example**:
        ```bash
        # Function to check if a number is even
        is_even() {
            local num=$1
            if [ $((num % 2)) -eq 0 ]; then
                echo "true"
            else
                echo "false"
            fi
        }
        echo $(is_even 4)  # Outputs "true"
        ```

2. **Improving Code Reusability**:
    - Functions can be reused multiple times throughout your script.
    - **Example**:
        ```bash
        # Function to calculate the factorial of a number
        factorial() {
            local num=$1
            if [ $num -le 1 ]; then
                echo 1
            else
                echo $(( num * $(factorial $((num - 1))) ))
            fi
        }
        echo "Factorial of 5: $(factorial 5)"
        ```

3. **Enhancing Code Readability**:
    - Functions improve the readability of your script by abstracting complex code blocks.
    - **Example**:
        ```bash
        # Function to find the maximum of two numbers
        find_max() {
            local num1=$1
            local num2=$2
            if [ $num1 -gt $num2 ]; then
                echo $num1
            else
                echo $num2
            fi
        }
        echo "The maximum number is: $(find_max 10 20)"
        ```

````markdown
# Simple Greeting Function

```bash
# Define the function
greet() {
    echo "Hello, $1!"
}

# Call the function
greet "Bob"
```

**Task**: Modify the function to accept a second parameter for the time of day (e.g., "morning") and print "Good morning, Bob!".

## Arithmetic Operations

```bash
calculate() {
    echo "Sum: $(( $1 + $2 ))"
    echo "Difference: $(( $1 - $2 ))"
    echo "Product: $(( $1 * $2 ))"
    echo "Quotient: $(( $1 / $2 ))"
}

calculate 10 5
```

**Task**: Add error handling for division by zero.

## Check Even or Odd

```bash
check_even_odd() {
    if [ $(( $1 % 2 )) -eq 0 ]; then
        echo "$1 is even."
    else
        echo "$1 is odd."
    fi
}

check_even_odd 7
```

**Task**: Modify the function to check a range of numbers passed as arguments.

## Factorial Calculation

```bash
factorial() {
    if [ $1 -le 1 ]; then
        echo 1
    else
        prev=$(factorial $(( $1 - 1 )))
        echo $(( $1 * prev ))
    fi
}

result=$(factorial 5)
echo "Factorial: $result"
```

**Task**: Add input validation to ensure the function receives a positive integer.

## Practice Exercises

### Function to Convert Celsius to Fahrenheit

```bash
celsius_to_fahrenheit() {
    fahrenheit=$(echo "scale=2; $1 * 9 / 5 + 32" | bc)
    echo "$1 Celsius is $fahrenheit Fahrenheit"
}

celsius_to_fahrenheit 25
```

### Function to Find the Maximum of Three Numbers

```bash
max_of_three() {
    if [ $1 -ge $2 ] && [ $1 -ge $3 ]; then
        echo "Max: $1"
    elif [ $2 -ge $1 ] && [ $2 -ge $3 ]; then
        echo "Max: $2"
    else
        echo "Max: $3"
    fi
}

max_of_three 3 9 5
```

### Function to Check Prime Numbers

```bash
is_prime() {
    n=$1
    if [ $n -le 1 ]; then
        echo "$n is not a prime number."
        return
    fi
    for ((i = 2; i <= n / 2; i++)); do
        if [ $((n % i)) -eq 0 ]; then
            echo "$n is not a prime number."
            return
        fi
    done
    echo "$n is a prime number."
}

is_prime 17

