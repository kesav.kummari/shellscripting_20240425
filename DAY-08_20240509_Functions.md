##### Session Video  

```
https://drive.google.com/file/d/1Y8f5SdHuHQo0-o-0WeMQqE8R49BrKnNo/view?usp=sharing
```

## Day 7: Advanced Concepts

### Topics Covered: Functions in shell scripts

### Functions

- **Definition**: Functions are named blocks of code that perform a specific task and can be reused throughout a script. Functions help in organizing code and avoiding repetition.

- **Syntax**:
    ```bash
    function_name() {
        # Code block to execute when the function is called
    }
    ```

- **Example**: Basic function that prints a greeting message:
    ```bash
    greet() {
        echo "Hello, welcome to shell scripting!"
    }
    greet
    ```

    ### Scope of Variables in Shell Scripts

In shell scripting, variables have a scope that determines their visibility and accessibility. There are two main types of variable scopes in shell scripts: global (or environment) variables and local variables.

#### Global (Environment) Variables

- **Definition**: Global variables, also known as environment variables, are available throughout the script and in any child processes spawned by the script.
- **Declaring**: By default, any variable you declare in a script is a global variable.
- **Exporting**: You can explicitly mark a variable as global using the `export` command. This allows child processes to inherit the variable.
- **Example**:
    ```bash
    export globalVar="I am global"
    echo $globalVar
    ```

#### Local Variables

- **Definition**: Local variables are limited to the function or block of code in which they are declared. They are not accessible outside of that function or block.
- **Declaring**: In Bash, you can declare a local variable within a function using the `local` keyword.
- **Example**:
    ```bash
    function exampleFunction {
        local localVar="I am local"
        echo $localVar
    }
    
    exampleFunction
    echo $localVar  # This will not print anything because localVar is out of scope.
    ```

#### Summary of Variable Scope

- **Global variables**: 
    - Are accessible throughout the script and any child processes.
    - Are the default scope of variables in shell scripts.
    - Can be explicitly set as global using the `export` command.

- **Local variables**: 
    - Are limited to the function or block in which they are declared.
    - Are not accessible outside of their defined scope.
    - Must be declared using the `local` keyword within a function.

#### Practice Exercise

To better understand the scope of variables, try the following exercise:

1. **Question**:
   Write a script with a function that declares a local variable. Then, try to access the local variable outside the function and observe the behavior.

2. **Solution**:
    ```bash
    function exampleFunction {
        local localVar="I am local"
        echo "Inside function: $localVar"
    }
    
    exampleFunction
    echo "Outside function: $localVar"  # This will not print the localVar value
    ```

